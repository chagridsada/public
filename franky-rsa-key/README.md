Franky rsa key
=====

* generate

```
curl -s -k https://bitbucket.org/chagridsada/public/raw/master/franky-rsa-key/generate.sh | bash
```

* generate and install

```
curl -s -k https://bitbucket.org/chagridsada/public/raw/master/franky-rsa-key/generate_install.sh | bash
```
