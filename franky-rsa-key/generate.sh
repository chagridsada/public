#!/bin/bash

## --- create dir ---
DIR="franky-rsa-key"
mkdir -p "$DIR"

## --- authorized_keys ---
AUTH=$(cat <<TMP
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDNZ36g2NZi4Y0qw70Rh7bqYZJOFxv8lVSlVWNhD5PVFasUFMI1Gn/xb/9YM2/X22KQOiRv7d00IMzFJm68LVdHweSjmWHUg79UHeYFqdlh63Dpx7jReJkjApiyViuZzgF8QtWKzepgnxOUYWxulnKVbk3Mjd9Dfy5WKfT80djfM3ABixJRmuqAXB/hLkIaxCw5WCSwLNSWVgMWrtkO7MJkqAo52ou+ie2t8eJbzXG3u9WynIFHzBDY9s+1lleATi+h0VP5gM3PCbHdWfsJnO0pGZYJyGd2LOU+jwFDKvblH2jLl8uuP0AcrURTryKsLBGNVJE9HKtjRndVqUFvTucv
TMP
)
echo "$AUTH" > "$DIR/authorized_keys"
echo "Generate: $DIR/authorized_keys"

## --- id_rsa ---
ID_RSA=$(cat <<TMP
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAnxashJDsuwB2/gVy5brbEQX9rmR8IHHiEh+5mN0J7dq8c8fP
mYdVM3qBTpDw9r6LbBg/u/umCWtjJ6IcV/3vhniKfzCaCWGN18Ve+hSMfQILJZ9X
q95XxoZ/HWvNmF8Sh4aW+LMiqXDRQ83H0YVfHRkHnGc8zlJV5ujYaz2sADV8PNNi
vllTqF/1lJoGZx/m3auo1di+hFhdeWbEDu+XVJidljPrT1WGRo4O/DmfN/JqcR78
2IgKWzGYFe/VbilWwjOINLHpW4olL7cywYvrDQzo3IgW1dCf+UHjDtjnKBhI/8JQ
hd9GXDl04Qh6DFFzEmu+LKMaUGHXScf/4p4gRwIDAQABAoIBAGyehIe2k4tSU+w4
OCyWGcBTwNLzFLTqTJpnQscWFBNsR0Md56ZBenC9cc/8sikmC/dvzwRAWysPQRHs
Y0HIxWOgpZ8VxfYlvzzVK3W/1fP8EzRKyQc1teR8LAogcq1YLESQJkADvuTFfHu4
4ncxgrFRR5sH+cI9AR25WRpPb0JsLlv31XxLX0sgH7VBbqw6vj/OQ/gIjtIPnpYk
vylCmpNMSQnyCkhIS6iICHob1klsE5kpnGerlGTnWEfByaRPF4eQxQ5V2iLz4qeO
w6opy9ndfgQ763/doV9Z66z5FRYzqjWcnBIl1o5XRUCCiw4FlQGp+dkoilKU/XIE
pwJ6ElECgYEA0znD7685HBumu95FXror/z8FAqePuAP9ZHywDa228ayEm0boSyBU
M2C2fCEoGQZ0PnEqHR/ju4cWJZEWQWlMQUcNR6GoeNy98/TwDfD8Ht9kM5ui981i
G0CJw4ySyBjxg5eSxwKYI/qhwyB0dUUSI6lZlT0auPv5jIJyAhR8wjUCgYEAwM+r
7UkiO7NPfkRYxk8JhEM2TEDpEJn46GiVrqrmZnvcJT3xT1GOzCuzERZK1jPb/lCY
bA30yB1lpPBrT8PGdpmeZLzDEPJsLoJu7XlfvVP+pA6j+E1ARdQmjxQIqf8X8IDI
NuUE4QV7howSA1VVvidotnW0I4XpNor0ONRzqAsCgYB+azxR39WWdRY9qrJqF5Lt
mZIuHItv3zNxSQ81oJq12kMjjDTLgfXjKicN8+pD0BESrjg+Zi+utRMUONnRHGx/
RT81eBOVAsjNkfN65JiBAcK/AI9xMINfPCk32JKhIAysbbNzWKImJoNafbo11bkd
juYG5CNyQ5z/87K4e/qPYQKBgDZalOIIiuD/lHpI9Cm7YDhr5iOq7MjkTvndeczK
mrbrVc0d4Cf3GuVNsGYxvZkS94KC3+UeLhI3z06nA+lxjEIk3jURzX+6h2q5FGJX
o/iwdAl1ox29UVAeFVyiTir2IZ6H/2kSOMTikccFN5K+/LPlUiyVk9mrx2m71Est
AQmvAoGBAKpPyrVSxYBR9wnKe40F6kzKCD/tE6PHRPrSm1DhKYjZPlTVsUXr3Par
mLI0TEr0Z6ne5g/BR1vSCrJ6eunthIIHI89+jDYe2C9JrUAOv/c5/n766eGHzkbv
E2pXxhR9sb3F9Dr9t5ZGphepae/QcPc8wpaH0r1+O1wsEMQ5Nwsq
-----END RSA PRIVATE KEY-----
TMP
)
echo "$ID_RSA" > "$DIR/id_rsa"
echo "Generate: $DIR/id_rsa"

## --- id_rsa.pub ---
ID_RSA_PUB=$(cat <<TMP
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCfFqyEkOy7AHb+BXLlutsRBf2uZHwgceISH7mY3Qnt2rxzx8+Zh1UzeoFOkPD2votsGD+7+6YJa2MnohxX/e+GeIp/MJoJYY3XxV76FIx9Agsln1er3lfGhn8da82YXxKHhpb4syKpcNFDzcfRhV8dGQecZzzOUlXm6NhrPawANXw802K+WVOoX/WUmgZnH+bdq6jV2L6EWF15ZsQO75dUmJ2WM+tPVYZGjg78OZ838mpxHvzYiApbMZgV79VuKVbCM4g0selbiiUvtzLBi+sNDOjciBbV0J/5QeMO2OcoGEj/wlCF30ZcOXThCHoMUXMSa74soxpQYddJx//iniBH franky.services.so
TMP
)
echo "$ID_RSA_PUB" > "$DIR/id_rsa.pub"
echo "Generate: $DIR/id_rsa.pub"

## --- change permission ---
chmod 600 "$DIR"/*

## EOF
