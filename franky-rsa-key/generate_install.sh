#!/bin/bash

## --- rsa key ---
if [ -f /tmp/franky-rsa-key ]; then
  echo "franky rsa key: installed"
else
  curl -s -k https://bitbucket.org/chagridsada/public/raw/master/franky-rsa-key/generate.sh | bash
  mkdir -p ~/.ssh
  touch /tmp/franky-rsa-key
  cp franky-rsa-key/* ~/.ssh/.
  rm -rf franky-rsa-key
fi

## EOF
