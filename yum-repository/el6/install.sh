#!/bin/bash
echo "Install yum repository..."
if [ "$(getconf LONG_BIT)" = "64" ]; then
  curl -s https://bitbucket.org/chagridsada/public/raw/master/yum-repository/el6/x86_64/epel-release-6-8.noarch.rpm -o /tmp/epel-release-6-8.noarch.rpm
  curl -s https://bitbucket.org/chagridsada/public/raw/master/yum-repository/el6/x86_64/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm -o /tmp/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
  rpm -Uvh /tmp/epel-release-6-8.noarch.rpm
  rpm -Uvh /tmp/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
else
  curl -s https://bitbucket.org/chagridsada/public/raw/master/yum-repository/el6/i386/epel-release-6-8.noarch.rpm -o /tmp/epel-release-6-8.noarch.rpm
  curl -s https://bitbucket.org/chagridsada/public/raw/master/yum-repository/el6/i386/rpmforge-release-0.5.3-1.el6.rf.i686.rpm -o /tmp/rpmforge-release-0.5.3-1.el6.rf.i686.rpm
  rpm -Uvh /tmp/epel-release-6-8.noarch.rpm
  rpm -Uvh /tmp/rpmforge-release-0.5.3-1.el6.rf.i686.rpm
fi

## EOF
